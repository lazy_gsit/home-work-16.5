#include <iostream>
#include <ctime>

using namespace std;

// Find out the day of the month
int Day()
{
    time_t now = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &now);
    int iday = timeinfo.tm_mday;
    return iday;
}

int main()
{
    // Output Cyrillic to console
    setlocale(LC_ALL, "ru");

    const int size = 10;
    
    int array[size][size];

    for (int row = 0; row < size; row++)
    {
        for (int col = 0; col < size; col++)
        {
            array[row][col] = row + col;
            cout << " " << array[row][col];
        }

        cout << endl;
    }

    int sum = 0;
    for (int row = 0; row < size; row++)
    {
        for (int col = 0; col < size; col++)
        {
            if (row == 20 % size)
            {
                sum += array[row][col];
            }
        }
    }
    cout << "����� ��������� ������ ����� " << sum << endl;

    
    return 0;
}
